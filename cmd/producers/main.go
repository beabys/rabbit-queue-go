package main

import (
	"encoding/json"
	"log"
	"math/rand"
	"time"

	"github.com/streadway/amqp"
	queue "gitlab.com/beabys/rabbit-queue-go"
)

type message struct {
	message1 string
	message2 string
}

func handleError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}

}

func main() {
	conn, err := amqp.Dial(queue.Connection.AmpConnectionString)
	handleError(err, "Can't connect to AMQP")
	defer conn.Close()

	amqpChannel, err := conn.Channel()
	handleError(err, "Can't create a amqpChannel")

	defer amqpChannel.Close()

	queue, err := amqpChannel.QueueDeclare("drivers", true, false, false, false, nil)
	handleError(err, "Could not declare `drivers` queue")

	rand.Seed(time.Now().UnixNano())

	queueMesssage := message{message1: "test1", message2: "test2"}
	body, err := json.Marshal(queueMesssage)
	if err != nil {
		handleError(err, "Error encoding JSON enqueue message")
	}

	err = amqpChannel.Publish("", queue.Name, false, false, amqp.Publishing{
		DeliveryMode: amqp.Persistent,
		ContentType:  "text/plain",
		Body:         body,
	})

	if err != nil {
		log.Fatalf("Error publishing message: %s", err)
	}

	log.Printf("AddTask: %s+%s", queueMesssage.message1, queueMesssage.message2)

}
