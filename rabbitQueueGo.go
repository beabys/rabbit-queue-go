package rabbit

type connection struct {
	AmpConnectionString string
}

// Connection return the connection string
// for amp (aka RabbitMq)
var Connection = connection{
	AmpConnectionString: "amqp://rabbitmquser:some_password@localhost:5672/",
}
